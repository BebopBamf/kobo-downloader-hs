{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

{- |
	Module      :  App
	Description :  The main library module for kobo-downloader-hs
	Copyright   :  2023 Euan Mendoza
	License     :  GNU GPL-3.0
	Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>

	Stability   :  experimental
	Portability :  portable
-}
module App (
    app,
    appSettings,
    App (..),
) where

-- import Control.Monad
import Control.Monad.IO.Class (MonadIO (..))
import Control.Monad.Reader (MonadReader)

import Data.Aeson (decode)
import Data.Text qualified as Text
import Data.Text.IO qualified as Text
import Data.Text.Lazy.Encoding qualified as TLE
import Data.Text.Lazy.IO qualified as TL
import Iris qualified
import Options.Applicative qualified as Opt
import System.Environment.XDG.BaseDir (getUserConfigFile)

import Paths_kobo_downloader_hs qualified as Autogen

import Kobo.Config

data ListOptions = ListOptions
    { listOptionsAll :: Bool
    , listOptionsHelp :: Bool
    }

data PickOptions = PickOptions
    { pickOptionsAll :: Bool
    , pickOptionsPath :: FilePath
    }

listOptionsP :: Opt.Parser ListOptions
listOptionsP = do
    listOptionsAll <-
        Opt.switch $
            mconcat
                [ Opt.long "all"
                , Opt.short 'a'
                , Opt.help "List all books, including those already downloaded"
                ]

    listOptionsHelp <-
        Opt.switch $
            mconcat
                [ Opt.long "help"
                , Opt.short 'h'
                , Opt.help "Show this help text"
                ]

    pure ListOptions{..}

pickOptionsP :: Opt.Parser PickOptions
pickOptionsP = do
    pickOptionsAll <-
        Opt.switch $
            mconcat
                [ Opt.long "all"
                , Opt.short 'a'
                , Opt.help "Pick all books, including those already downloaded"
                ]

    pickOptionsPath <-
        Opt.argument
            Opt.str
            ( mconcat
                [ Opt.metavar "PATH"
                , Opt.help "The path to download the books to"
                ]
            )

    pure PickOptions{..}

data AppMode
    = AppModeInfo
    | AppModeList ListOptions
    | AppModePick PickOptions
    | AppModeGet
    | AppModeWishlist

modeParser :: Opt.Parser AppMode
modeParser =
    Opt.subparser
        ( Opt.command
            "info"
            ( Opt.info
                (pure AppModeInfo)
                (Opt.progDesc "Show the location of the config file")
            )
            <> Opt.command
                "list"
                ( Opt.info
                    (AppModeList <$> listOptionsP)
                    (Opt.progDesc "List all books in your library")
                )
            <> Opt.command
                "pick"
                ( Opt.info
                    (AppModePick <$> pickOptionsP)
                    (Opt.progDesc "Manually select books from your library")
                )
            <> Opt.command
                "get"
                ( Opt.info
                    (pure AppModeGet)
                    (Opt.progDesc "Download books from your library")
                )
            <> Opt.command
                "wishlist"
                ( Opt.info
                    (pure AppModeWishlist)
                    (Opt.progDesc "List all books in your wishlist")
                )
        )

newtype App a = App
    { unApp :: Iris.CliApp AppMode () a
    }
    deriving newtype
        ( Functor
        , Applicative
        , Monad
        , MonadIO
        , MonadReader (Iris.CliEnv AppMode ())
        )

appSettings :: Iris.CliEnvSettings AppMode ()
appSettings =
    Iris.defaultCliEnvSettings
        { Iris.cliEnvSettingsHeaderDesc =
            "Kobo EBook Downloader  "
                ++ "Copyright (C) 2023  Euan Mendoza"
        , Iris.cliEnvSettingsProgDesc =
            "A CLI tool used to download ebooks from Kobo's server "
                ++ "and remove the DRM."
        , Iris.cliEnvSettingsVersionSettings =
            Just
                (Iris.defaultVersionSettings Autogen.version)
                    { Iris.versionSettingsMkDesc = ("Kobo EBook Downloader Version: " <>)
                    }
        , Iris.cliEnvSettingsCmdParser = modeParser
        }

configPath :: IO FilePath
configPath = getUserConfigFile "" "kobo-book-downloader.json"

copyrightDisclaimer :: IO ()
copyrightDisclaimer = do
    Text.putStrLn "kobo-downloader  Copyright (C) 2023  Euan Mendoza"
    Text.putStrLn "This program comes with ABSOLUTELY NO WARRANTY."
    Text.putStrLn "This is free software, and you are welcome to redistribute it"
    Text.putStrLn "under certain conditions; see the GNU General Public License"
    Text.putStrLn "for details."
    Text.putStrLn ""
    Text.putStrLn ""

info :: App ()
info = liftIO $ do
    Text.putStrLn "The configuration file is located at:"
    configPath >>= Text.putStrLn . Text.pack

list :: ListOptions -> App ()
list ListOptions{listOptionsAll = true} = do
    liftIO $ Text.putStrLn "List all"
    liftIO $ Text.putStrLn "List some"

pick :: PickOptions -> App ()
pick PickOptions{..} = do
    liftIO $ putStrLn "Pick"

getDeviceId :: Maybe Config -> Text.Text
getDeviceId = maybe "No Device ID" configDeviceId

app :: App ()
app = do
    liftIO copyrightDisclaimer

    appMode <- Iris.asksCliEnv Iris.cliEnvCmd

    contents <- liftIO $ TL.readFile =<< configPath
    liftIO $ TL.putStrLn contents

    let obj = decode $ TLE.encodeUtf8 contents :: Maybe Config

    liftIO $ do
        Text.putStrLn "Config DeviceId:"
        Text.putStrLn $ getDeviceId obj

    case appMode of
        AppModeInfo -> info
        AppModeList args -> list args
        AppModePick args -> pick args
        AppModeGet -> do
            liftIO $ putStrLn "Get"
        AppModeWishlist -> do
            liftIO $ putStrLn "Wishlist"
