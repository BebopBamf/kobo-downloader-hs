{- |
	Module      :  Kobo.Data
	Description :  The main library module for kobo-downloader-hs
	Copyright   :  2023 Euan Mendoza
	License     :  GNU GPL-3.0
	Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>

	Stability   :  experimental
	Portability :  portable
-}
module Kobo.Data where