{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ImportQualifiedPost #-}

module Kobo.Config (Config (..)) where

import GHC.Generics

import Data.Aeson (
    FromJSON (parseJSON),
    Options (..),
    ToJSON (toEncoding, toJSON),
    defaultOptions,
    genericParseJSON,
    genericToEncoding,
    genericToJSON,
 )
import Data.Text qualified as Text
import Text.Casing (pascal)

data Config = Config
    { configDeviceId :: Text.Text
    , configAccessToken :: Text.Text
    , configRefreshToken :: Text.Text
    , configUserId :: Text.Text
    , configUserKey :: Text.Text
    }
    deriving (Generic, Show)

instance FromJSON Config where
    parseJSON =
        genericParseJSON
            defaultOptions{fieldLabelModifier = pascal . drop 6}

instance ToJSON Config where
    toJSON =
        genericToJSON
            defaultOptions{fieldLabelModifier = pascal . drop 6}

    toEncoding =
        genericToEncoding
            defaultOptions{fieldLabelModifier = pascal . drop 6}