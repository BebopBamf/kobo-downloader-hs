{- |
  Module      :  Main
  Description :  The application for kobo-downloader-hs
  Copyright   :  2023 Euan Mendoza
  License     :  GNU GPL-3.0
  Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>

  Stability   :  experimental
  Portability :  portable
-}
module Main (main) where

import qualified Iris

import App

main :: IO ()
main = Iris.runCliApp appSettings $ unApp app
